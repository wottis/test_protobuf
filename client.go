package main

import (
  "fmt"
  "flag"
  "os"
  "encoding/csv"
  "ProtobufTest"
  "github.com/golang/protobuf/proto"
)

const CLIENT_NAME = "GoClient"
const CLIENT_ID = 2
const CLIENT_DESCRIPTION = "This is a Go Protobuf client!!"

type Headers []string

func (h Headers) getHeaderIndex(headername string) int {
  if len(headername) >= 2{
    for index, s := range h {
      if s == headername {
        return index
      }
    }
  }
  return -1
}

func checkError(err error) {
  if err != nil {
    fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
    os.Exit(1)
  }
}

func retrieveDataFromFile(fname *string) ([]byte, error) {
  file, err := os.Open(*fname)
  checkError(err)
  defer file.Close()  // Close fp when we're done with that func.
  csvreader := csv.NewReader(file)

  // Get the csv headers.
  var hdrs Headers
  hdrs, err = csvreader.Read()
  checkError(err)

  // Get indexes and set first message fields for client.
  ITEMIDINDEX := hdrs.getHeaderIndex("itemid")
  ITEMNAMEINDEX := hdrs.getHeaderIndex("itemname")
  ITEMVALUEINDEX := hdrs.getHeaderIndex("itemvalue")
  ITEMTYPEINDEX := hdrs.getHeaderIndex("itemType")
  ProtoMessage := new(ProtobufTest.TestMessage)
  ProtoMessage.ClientName = proto.String(CLIENT_NAME)
  ProtoMessage.ClientId = proto.Int32(CLIENT_ID)
  ProtoMessage.Description = proto.String(CLIENT_DESCRIPTION)
}

func main() {
  filename := flag.String("f", "CSVValue.csv", "Enter the filename to read from")
  dest := flag.String("d", "127.0.0.1:2110", "Enter the servers ip address")
  flag.Parse()

  data, err := retreiveDataFromFile(filename)
  checkError(err)
  sendDataToDest(data, dest)
}
